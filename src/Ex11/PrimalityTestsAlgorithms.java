package Ex11;

import java.math.BigInteger;
/**
 * Solution for exercise 1
 * @author Rania
 *
 */
public class PrimalityTestsAlgorithms {
	public static void main(String[] args) {
		System.out
				.println("Running strong primality tests on the following numbers..");
		// solution for Exercise 1.iii
		System.out.println("x = 2, N=41 ==>" + isPsuedoPrimeStrong(2, 41));
		// solution for Exercise 1.iv
		System.out.println("x = 37, N=57 ==>" + isPsuedoPrimeStrong(37, 57));
		// solution for Exercise 1.v
		System.out.println("x = 47, N=1105 ==>" + isPsuedoPrimeStrong(47, 1105));
		// solution for Exercise 1.vi
		System.out.println("x = 2, N=1105 ==>" + isPsuedoPrimeStrong(2, 1105));
		System.out.println("******");
		// solution for Exercise 1.vii
		//System.out.println("Finding the Fermat liars..");
		System.out.println("Number of Fermat liars for N=35 is:"
				+ findFermatLiars(35));
		// solution for Exercise 1.viii
		//System.out.println("Finding strong liars..");
		System.out.println("Number of strong liars for N=35 is:"
				+ findStrongLiars(35));
		
		// solution for Exercise 1.ix
		System.out.println("Number of Fermat liars for N=561 is:"
				+ findFermatLiars(561));
		// solution for Exercise 1.viii
		//System.out.println("Finding strong liars..");
		System.out.println("Number of strong liars for N=561 is:"
				+ findStrongLiars(561));
	}
	// solution for 1.vii
	public static int findFermatLiars(int N) {
		if (isCertainlyPrime(N))
			return 0;
		int liarsCount = 0;
		for (int x = 1; x < N; x++) {
			if (isPrimeByFermat(x, N)) {
					++liarsCount;
			}
		}
		return liarsCount;
	}
	
	
	public static int findStrongLiars(int N) {
		if (isCertainlyPrime(N))
			return 0;
		int liarsCount = 0;
		for (int x = 1; x < 35; x++) {
			if (isPsuedoPrimeStrong(x, N)) {
					++liarsCount;
				}
		}
		return liarsCount;
	}

	// we allow ourselves to use this brute force algorithm because we are
	// testing with small numbers
	public static boolean isCertainlyPrime(int N) {
		int i = 2;
		while (i < Math.sqrt(N)) {
			if (N % i == 0) {
				return false;
			}
			++i;
		}
		return true;
	}

	// solution for Exercise 1.i: implement primality test using Fermat's
	// theorem (Test whether x exp(N-1)modN=1
	// a false return value means N is certainly composite, true
	// means N is probably prime
	public static boolean isPrimeByFermat(int groupElem, int N) {
		if (gcd(groupElem, N) != 1)
			return false;
		BigInteger exp = BigInteger.valueOf(N - 1);
		// execute groupElem Exp(N-1) mod N
		BigInteger modulo = (BigInteger.valueOf(groupElem)).modPow(exp,
				BigInteger.valueOf(N));
		// System.out.print(groupElem + "exp" + (N - 1) + "mod" + N + "=" +
		// modulo);
		return (modulo.equals(BigInteger.ONE)) ? true : false;
	}

	// Exercise 1.ii: implement strong pseudoprimality test
	// a false return value means N is certainly composite, true
	// means N is probably prime
	public static boolean isPsuedoPrimeStrong(int groupElem, int N) {
		if (gcd(groupElem, N) != 1)
			return false;
		// find e, m such that p-1=2^e*m
		int[] e_m = findCompositePower(N - 1);
		int e = e_m[0];
		int m = e_m[1];
		// y = groupElem^m mod N
		BigInteger y = (BigInteger.valueOf(groupElem)).modPow(
				BigInteger.valueOf(m), BigInteger.valueOf(N));
		// ymodN=-1 <==> ymodN=N-1
		if (y.equals(BigInteger.ONE) || y.equals(BigInteger.valueOf(N - 1))) {
			//System.out.println(N
				//	+ " is probably prime by the strong primality test.");
			return true;
		}
		for (int i = 0; i < e; i++) {
			y = y.modPow(BigInteger.valueOf(2), BigInteger.valueOf(N));
			if (y.equals(BigInteger.valueOf(N - 1))) {
				//System.out.println(testNum
					//	+ " is probably prime by the strong primality test..");
				return true;
			}
		}
		//System.out.println(testNum + " is certainly composite");
		return false;
	}

	// find e,m such that p_1=2^e*m (return array [e, m])
	private static int[] findCompositePower(int p_1) {
		// log2(p-1)
		int m = (int) (Math.log10(p_1) / Math.log10(2));
		for (int e = m; e >= 1; e--)
			if ((int) ((p_1) % Math.pow(2, e)) == 0)
				return new int[] { e, (int) ((p_1) / Math.pow(2, e)) };
		return null;

	}

	// return gcd of p and q
	private static int gcd(int p, int q) {
		if (q == 0)
			return p;

		int val = gcd(q, p % q);
		return val;
	}

}
