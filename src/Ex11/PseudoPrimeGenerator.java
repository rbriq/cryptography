package Ex11;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PseudoPrimeGenerator {

	private static Random rnd = new SecureRandom();

	public static void main(String[] args) {
		int primeLength = 1027;
		BigInteger psuedoP = generatePrime(primeLength);
		System.out.println(psuedoP);
		System.out.println(isPsuedoPrimeStrong(BigInteger.valueOf(23232121), psuedoP));
	}

	private static BigInteger generatePrime(int primeLength) {
		BigInteger x = BigInteger.valueOf(2).pow(primeLength - 1);
		BigInteger N = null;
		BigDecimal xsqrtD = new BigDecimal(x).multiply(
				new BigDecimal(Math.sqrt(2)));
		
		BigInteger xsqrtI = xsqrtD.toBigInteger();
		
		int passed = 0;
		int t = 5; // t is the security parameter
		 while (passed < t) {
			N = generateRnd(primeLength-1, xsqrtI);
			if(N.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO)) {
				//System.out.println("cont.");
				continue;
			}
			List<BigInteger> rndNumbers = generateRandomNumbers(N, primeLength,
					t);
			for (BigInteger base : rndNumbers) {
				if (isPsuedoPrimeStrong(base, N)) {
					//System.out.println("passed:" + passed);
					++passed;
				}
				else {
					passed = 0;
					//System.out.println("not passed");
					break;
				}
			}
		}
		return N;
	}

	public static boolean isPsuedoPrimeStrong(BigInteger base, BigInteger n) {
		if (!base.gcd(n).equals(BigInteger.ONE))
			return false;
		// find e, m such that p-1=2^e*m
		BigInteger[] e_m = findCompositePower(n.subtract(BigInteger.ONE));
		BigInteger e = e_m[0];
		BigInteger m = e_m[1];
		// y = groupElem^m mod N
		BigInteger y = base.modPow(m, n);
		// ymodN=-1 <==> ymodN=N-1
		if (y.equals(BigInteger.ONE) || y.equals(n.subtract(BigInteger.ONE))) {
			// System.out.println(N
			// + " is probably prime by the strong primality test.");
			return true;
		}
		for (BigInteger i = BigInteger.ZERO; i.compareTo(e) < 0; i = i
				.add(BigInteger.ONE)) {
			y = y.modPow(BigInteger.valueOf(2), n);
			if ( y.equals(n.subtract(BigInteger.ONE))) {
				return true;
			}
		}
		// System.out.println(testNum + " is certainly composite");
		return false;
	}

	public static List<BigInteger> generateRandomNumbers(BigInteger N,
			int bitsNumber, int t) {
		List<BigInteger> rndNumbers = new ArrayList<BigInteger>();
		while (rndNumbers.size() < t) {
			BigInteger curRndNum = new BigInteger(bitsNumber, rnd).add(N)
					.mod(N);
			if (!rndNumbers.contains(curRndNum)) {
				rndNumbers.add(curRndNum);
			}
		}
		return rndNumbers;
	}
	
	// generate a random number in the range x and xqrtD
		private static BigInteger generateRnd(int primeBits, BigInteger xsqrtI) {
			BigInteger rndNum = new BigInteger(primeBits, rnd).add(xsqrtI)
					.mod(xsqrtI);
			return rndNum;
		}

	// find e,m such that p_1=2^e*m (return array [e, m])
	private static BigInteger[] findCompositePower(BigInteger p_1) {
		int log = 1;
		BigInteger dev  = p_1;
		while (dev.divide(BigInteger.valueOf(2)).compareTo(BigInteger.ONE) > 0) {
			log++;
			dev = dev.divide(BigInteger.valueOf(2));
		}
		for (int e = log; e > 0; e--) {
			if (((p_1).mod(BigInteger.valueOf(2).pow(e)).equals(BigInteger.ZERO)))
				return new BigInteger[] { BigInteger.valueOf(e),
						p_1.divide(BigInteger.valueOf(2).pow(e)) };
		}
		return null;

	}
}
