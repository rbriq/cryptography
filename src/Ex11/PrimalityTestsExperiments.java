package Ex11;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PrimalityTestsExperiments {

	private static Random rnd = new Random();
	public static void main(String[] args) {
		int count = 3;
		int N = 8911;
		
		List<Integer> randomNumbers = generateRandomNumbers(N, count);
		for (int x : randomNumbers) {
			if (!isPsuedoPrimeStrong(x, N)) {
				System.out.println("N is definitely composite");
				return;
			}
		}
		System.out.println("N seems to be prime after " + count
				+ " runs on different numbers");

	}

	public static List<Integer> generateRandomNumbers(int N, int count) {
		List<Integer> rndNumbers = new ArrayList<Integer>();
		while (rndNumbers.size() < count) {
			int curRndNum = (rnd.nextInt(N) + 1) % N;
			if (!rndNumbers.contains(curRndNum)) {
				rndNumbers.add(curRndNum);
				System.out.println("random: " + curRndNum);
			}
		}
		return rndNumbers;
	}

	// solution for Exercise 1.i: implement primality test using Fermat's
	// theorem (Test whether x exp(N-1)modN=1
	// a false return value means N is certainly composite, true
	// means N is probably prime
	public static boolean isPrimeByFermat(int groupElem, int N) {
		if (gcd(groupElem, N) != 1)
			return false;
		BigInteger exp = BigInteger.valueOf(N - 1);
		// execute groupElem Exp(N-1) mod N
		BigInteger modulo = (BigInteger.valueOf(groupElem)).modPow(exp,
				BigInteger.valueOf(N));
		// System.out.print(groupElem + "exp" + (N - 1) + "mod" + N + "=" +
		// modulo);
		return (modulo.equals(BigInteger.ONE)) ? true : false;
	}

	// Exercise 1.ii: implement strong pseudoprimality test
	// a false return value means N is certainly composite, true
	// means N is probably prime
	public static boolean isPsuedoPrimeStrong(int groupElem, int N) {
		if (gcd(groupElem, N) != 1)
			return false;
		// find e, m such that p-1=2^e*m
		int[] e_m = findCompositePower(N - 1);
		int e = e_m[0];
		int m = e_m[1];
		// y = groupElem^m mod N
		BigInteger y = (BigInteger.valueOf(groupElem)).modPow(
				BigInteger.valueOf(m), BigInteger.valueOf(N));
		// ymodN=-1 <==> ymodN=N-1
		if (y.equals(BigInteger.ONE) || y.equals(BigInteger.valueOf(N - 1))) {
			// System.out.println(N
			// + " is probably prime by the strong primality test.");
			return true;
		}
		for (int i = 0; i < e; i++) {
			y = y.modPow(BigInteger.valueOf(2), BigInteger.valueOf(N));
			if (y.equals(BigInteger.ONE) || y.equals(BigInteger.valueOf(N - 1))) {
				// System.out.println(testNum
				// + " is probably prime by the strong primality test..");
				return true;
			}
		}
		// System.out.println(testNum + " is certainly composite");
		return false;
	}

	// find e,m such that p_1=2^e*m (return array [e, m])
	private static int[] findCompositePower(int p_1) {
		// log2(p-1)
		int m = (int) (Math.log10(p_1) / Math.log10(2));
		for (int e = m; e >= 1; e--)
			if ((int) ((p_1) % Math.pow(2, e)) == 0)
				return new int[] { e, (int) ((p_1) / Math.pow(2, e)) };
		return null;

	}

	// return gcd of p and q
	private static int gcd(int p, int q) {
		if (q == 0)
			return p;

		int val = gcd(q, p % q);
		return val;
	}

}
