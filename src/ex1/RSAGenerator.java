package ex1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.SecureRandom;

public class RSAGenerator {

	public static void main(String[] args) {
		// Q2: part a
		//generatePrivRSAKey("Alice");
		//System.out.println();
		//generatePrivRSAKey("Bob");

		// part b:
		//String signedMessage = signMessageByAlice();
		//verify signature
		//verifySignature();
		
		// part c
		//createAndEncryptKey();
		String message = "I'm a meaningful message that would like to be ecncrypted please";
		byte[] aesinput512 = message.getBytes();
		System.out.println(aesinput512.length);
	}

	private static String createAndEncryptKey() {
		String generateKeyCmd = "openssl rand -base64 16 > key.bin";
		execCommand(generateKeyCmd);
		String encryptKeyCmd = "openssl rsautl -encrypt -inkey AlicePrivkey.pub.pem -pubin -in key.bin -out key.bin.enc";
		return execCommand(encryptKeyCmd);
	}

	public static boolean verifySignature() {
		String cmd = " openssl rsautl -verify -in sig -inkey AlicePrivkey.pem";
		execCommand(cmd);
		return true;
	}
	public static String signMessageByAlice() {
		String cmd = "openssl rsautl -sign -in input -inkey AlicePrivkey.pem -out sig";
		return execCommand(cmd);
	}

	public static void generatePrivRSAKey(String name) {
		String cmd = "openssl genrsa -out " + name + " Privkey.pem 2048";
		execCommand(cmd);
	}

	private static String execCommand(String cmd) {
		try {
			Process p = Runtime.getRuntime().exec("cmd");
			p.waitFor();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			String line = "";
			StringBuffer sb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
