package ex6;

import java.math.BigInteger;
import java.util.Random;
/**
 * I did not find it necessary to split into further classes since the point is only to 
 * simualte the steps of the algorithm and there's no remote connection involved between
 * the 2 parties who want to establish the trust. 
 * @author Rania
 *
 */
public class SchnorrAlgorithm {

	public static void main(String args[]) {
		SchnorrAlgorithm schnorr = new SchnorrAlgorithm();
		schnorr.generate_p_q_g();
		schnorr.generateSecret_a_b();
		BigInteger A = schnorr.computeA();
		BigInteger B = schnorr.computeB();
		BigInteger c = schnorr.issueChallenge();
		BigInteger r = schnorr.computeChallengeResponse(c);
		boolean verificationSucceeded = schnorr.verifyChallengeResponse(A, B,
				c, r);
		System.out.println("the verification result is "
				+ verificationSucceeded);

	}

	private BigInteger q;
	private BigInteger p;
	private BigInteger secreta;
	private BigInteger secretb;
	private final static int qBitsNumber = 160;
	private final static int pBitsNumber = 1024;
	private final static int certainty = 10;
	// the security parameter for the random challenge number sent to Alice
	private final static int security = 20;
	private BigInteger g;
	private Random rnd = new Random();

	public SchnorrAlgorithm() {

	}
	// generate p, q, g randomly such that they meet the requirements stated in terms of bits length
	//
	public void generate_p_q_g() {
		int bitsDiff = pBitsNumber - qBitsNumber;
		// generate a random prime of length 
		q = BigInteger.probablePrime(pBitsNumber, rnd);
		// find p = q*p_1+1, loop until you find such dividor p_1
		while (true) {
			BigInteger p_1 = new BigInteger(bitsDiff, rnd);
			p = p_1.multiply(q).add(BigInteger.ONE);
			// p is prime with certainty (1-(0.5)exp(certainty)
			if (p.bitLength() > (pBitsNumber -1) && p.isProbablePrime(certainty)) {
				System.out.println("p = q *" + p_1 + " + 1 ");
				break;
			}
		}
		do {
		g = BigInteger.probablePrime(qBitsNumber, rnd);
		}
		while(g.compareTo(q) >= 0);
		System.out.println("q value is: " + q);
		System.out.println("p value is: " + p);
		System.out.println("g value is: " + g);
	}

	public void generateSecret_a_b() {
		do {
		secreta = new BigInteger(qBitsNumber, rnd);
		}
		while(secreta.compareTo(q) >= 0);
		do {
			secretb = new BigInteger(qBitsNumber-1, rnd);
		} 
		while (secreta == secretb || secretb.compareTo(q) >= 0);
		System.out.println("secret a value is: " + secreta);
		System.out.println("secret b value is: " + secretb);
	}

	public BigInteger computeA() {
		BigInteger A = g.modPow(secreta, p);
		System.out.println("A value is: " + A);
		return A;
	}

	public BigInteger computeB() {
		BigInteger B = g.modPow(secretb, p);
		System.out.println("B value is: " + B);
		return B;
	}

	public BigInteger issueChallenge() {
		BigInteger challenge = new BigInteger(security, rnd);
		System.out.println("challenge value is: " + challenge);
		return challenge;
	}

	public BigInteger computeChallengeResponse(BigInteger challenge) {
		BigInteger response = secretb.add(secreta.multiply(challenge));
		System.out.println("challenge response is: " + response);
		return response;
	}

	public boolean verifyChallengeResponse(BigInteger A, BigInteger B,
			BigInteger c, BigInteger response) {
		BigInteger g_exp_r = g.modPow(response, p);
		BigInteger B_A_exp_c = B.multiply(A.modPow(c, p)).mod(p);
		System.out.println("g exp r = " + g_exp_r);
		System.out.println("B * (A exp c) = " + B_A_exp_c);
		return g_exp_r.equals(B_A_exp_c);

	}

	public BigInteger getQ() {
		return q;
	}

	public BigInteger getP() {
		return p;
	}

}
