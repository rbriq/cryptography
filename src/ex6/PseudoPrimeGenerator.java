package ex6;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PseudoPrimeGenerator {

	private static Random rnd = new SecureRandom();

	public static void main(String[] args) {
		int primeLength = 1027;
		//BigInteger psuedoP = generatePrime(primeLength);
		//System.out.println(psuedoP);
		int x = 2;
		System.out.println(isPsuedoPrimeStrong(BigInteger.valueOf(2), BigInteger.valueOf(41)));
	}

	

	
	public static boolean isPsuedoPrimeStrong(BigInteger base, BigInteger n) {
		if (!base.gcd(n).equals(BigInteger.ONE))
			return false;
		// find e, m such that p-1=2^e*m
		BigInteger[] e_m = findCompositePower(n.subtract(BigInteger.ONE));
		BigInteger e = e_m[0];
		BigInteger m = e_m[1];
		// y = groupElem^m mod N
		BigInteger y = base.modPow(m, n);
		// ymodN=-1 <==> ymodN=N-1
		if (y.equals(BigInteger.ONE) || y.equals(n.subtract(BigInteger.ONE))) {
			// System.out.println(N
			// + " is probably prime by the strong primality test.");
			return true;
		}
		for (BigInteger i = BigInteger.ZERO; i.compareTo(e) < 0; i = i
				.add(BigInteger.ONE)) {
			y = y.modPow(BigInteger.valueOf(2), n);
			if (y.equals(BigInteger.ONE)
					|| y.equals(n.subtract(BigInteger.ONE))) {
				return true;
			}
		}
		// System.out.println(testNum + " is certainly composite");
		return false;
	}

	public static List<BigInteger> generateRandomNumbers(BigInteger N,
			int bitsNumber, int t) {
		List<BigInteger> rndNumbers = new ArrayList<BigInteger>();
		while (rndNumbers.size() < t) {
			BigInteger curRndNum = new BigInteger(bitsNumber, rnd).add(N)
					.mod(N);
			if (!rndNumbers.contains(curRndNum)) {
				rndNumbers.add(curRndNum);
			}
		}
		return rndNumbers;
	}

	// find e,m such that p_1=2^e*m (return array [e, m])
	private static BigInteger[] findCompositePower(BigInteger p_1) {
		int log = 1;
		BigInteger dev  = p_1;
		while (dev.divide(BigInteger.valueOf(2)).compareTo(BigInteger.ONE) > 0) {
			log++;
			dev = dev.divide(BigInteger.valueOf(2));
		}
		for (int e = log; e > 0; e--) {
			if (((p_1).mod(BigInteger.valueOf(2).pow(e)).equals(BigInteger.ZERO)))
				return new BigInteger[] { BigInteger.valueOf(e),
						p_1.divide(BigInteger.valueOf(2).pow(e)) };
		}
		return null;

	}
}
