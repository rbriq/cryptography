package ex8;

import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class AESExample {

	private static Random random = new Random();
	public final static int length = 28;
	private final static int blockLength = 16;
	public static void main(String[] args) throws Exception {

		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128);
		final double normalizer = 1000000000;
		SecretKey secKey = keyGen.generateKey();
		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
		System.out.printf("%-15s %-25s %-25s %n", "i", "average[sec]",
				"deviation[sec]");
		// System.out.println("i                   average                           deviation ");
		for (int i = 0; i < length; i++) {
			double sum = 0.0;
			int powerI = (int) Math.pow(2, i);
			double[] timeValues = new double[powerI];
			// I avoided generating the complete input at a time 	
			// for the deviation calculation where I need to know
			//the individual encryption time for each block.

			for (int j = 0; j < powerI; j++) {
				double startTime = System.nanoTime();
				byte[] byteText = generateBlock();
				//System.out.println(new String(byteText));
				aesCipher.doFinal(byteText);
				double endTime = System.nanoTime();
				double elapsedTime = endTime - startTime;
				timeValues[j] = elapsedTime;
				sum += elapsedTime;
			}
			double mean = sum / powerI;
			System.out.printf("%-15s %-25s %-25s %n", i, mean / normalizer,
					calculateStd(timeValues, mean) / normalizer);
		}
	}

	public static double calculateStd(double[] values, double mean) {
		double devSum = 0;
		for (int i = 0; i < values.length; i++) {
			devSum += (values[i] - mean) * (values[i] - mean);
		}
		double std = Math.sqrt(devSum / (values.length));
		return std;
	}
	
	// randomly generate an input block
	public static byte[] generateBlock() {
		 byte blockBytes[] = new byte[blockLength];
	      random.nextBytes(blockBytes);
	      return blockBytes;
	}
}
