package ex8;

import java.math.BigInteger;
import java.util.Random;

public class Q3 {
	public static void main(String[] args) throws Exception {

		// generate a random prime of length
		Random rnd = new Random();
		BigInteger p = new BigInteger(
				"107313728214633881402529727601234051403339214228664318228594610689786788510081514444448995981953428599841775383351951139720719345087913170517242877080174958539637745468107816500403651171504387721743806870756270010931915093460113178239400149273770492545819805495452964968476117438596882036667823702963803652097");
		BigInteger p_1 = p.subtract(BigInteger.ONE);
		// find p = q*p_1+1, loop until you find such dividor p_1
		while (true) {
			BigInteger q = BigInteger.probablePrime(1023, rnd);
			if (p_1.mod(q).equals(BigInteger.ZERO)) {
				System.out.println(q);
				break;
			}

		}
	}
}
